---
tags: recap
---
# Stand vorher
[[Session 32]]
# Kamen vor
[[Ganimedes]], [[Vincent]], [[Mirul]], [[Cali Bloom]], [[Yaku Purisqan]], [[Neldon]], [[Ludwig Vanderhem]], [[Lec]], [[Ter]], [[Lydia]]
# Quests
[[Quest Körpertausch]], [[Quest Lydias Tagebuch]]
# Zusammenfassung
Portal führt zu einem runden Raum. Tür führt in einen Wartebereich. [[Neldon]] ist noch nicht hinterher gekommen. [[Ganimedes]] ist ein beleibter Halbling mit blonden Locken. Er trägt ein Zirkus Zauberer Outfit. Er bietet an eine Seele z.B. [[Vincent]] im Dachs entweder in eine Leiche zu verfrachten oder in einen neuen Körper. Dafür sollen wir ein Buch (Tagebuch) von seiner alten Mentorin [[Lydia]] zu organisieren. Sie ist mittlerweile tot und ihr Tagebuch enthält wichtige magische Geheimnisse. Besprechung: [[Neldon]] ist tendenziell dagegen sich in Gefahr zu bringen. [[Yaku Purisqan]] steht auf Keiros. Entscheidung: Wir holen das Buch, dafür bekommen wir eine Leiche für [[Vincent]] und [[Fabia Sandig]] bekommt einen gefallen. Er kennt [[Fabia Sandig]] nicht. [[Atlas]] gehört das Portal durch das wir hergekommen sind. Er ist ein Mensch, groß blaue Augen. Vertrag mit [[Ganimedes]]:

Vertrag [[Ganimedes]] gibt uns eine Leiche mit oder ohne kleinen finger an der rechten hand aber sonst unversehrt, die most recently gestorben ist, zwischen 20 und 50, einen Körper, der [[Vincent]] ein langes erfülltes leben ermöglicht. wir geben unser bestes, das buch in dem zustand, in dem wir es finden, zurückzubringen, wir sind nicht verantwortlich für den zustand des buches. wir möchten bitte eine Karte von der Umgebung haben [[Ganimedes]] schuldet [[Fabia Sandig]] einen gefallen. zu keinem Zeitpunkt versucht [[Ganimedes]], uns an seine Göttin zu opfern oder uns einen Finger zu klauen. zu keinem Zeitpunkt versuchen wir, ihn an [[Keiros]] zu opfern.
[[Ganimedes]], [[Ludwig Vanderhem]], [[Mirul]], [[Yaku Purisqan]] und [[Cali Bloom]] unterschreiben. Falls vertrag gebrochen wird stirbt die brechende Person. Wir finden einen Tiefling Körper für [[Vincent]]

Wir übernachten in einer Herberge in einem nahegelegenen Dorf. Es gibt viele Halblinge. Alle in dem Dorf haben ein begrenztes 
Am nächsten Tag brechen wir auf um [[Quest Lydias Tagebuch]] zu finden. Dazu bekommen wir [[Lec]] und [[Ter]] zwei Esel (die uns aber nicht wirklich schneller machen). Wir kommen an die Elbe (80 m breiten Fluss mit doller Strömung). Wir überqueren den Fluss mit der Back of Holding Cali Kombo. Auf der anderen Seite schlagen wir bald unser Camp auf. [[Cali Bloom]] sieht Irrlichter. Erste Wache verläuft gut. Zweite Wache [[Mirul]] und [[Ludwig Vanderhem]] klauen alles Seil, binden sich zusammen an Neldon und folgen den Irrlichtern. [[Ludwig Vanderhem]] versucht sie anzufassen und wird angegriffen. Die Irrlichter umrunden die beiden und einige Gule greifen an. [[Mirul]] weckt [[Neldon]] auf. [[Ludwig Vanderhem]] weckt die anderen beiden. Der Kampf ist kurz und nicht sehr anspruchsvoll.
# Punkte für nächste Session
Weiter auf der [[Quest Lydias Tagebuch]] um [[Ganimedes]] Job zu erfüllen
# Notizen