---
tags: recap
---
# Stand vorher
Es ist unklar, wer in welchem Körper steckt. Reihenfolge in der der Raum betreten wurde Wildschwein, [[Mirul]], [[Mirul]], [[Yaku Purisqan]], [[Ludwig Vanderhem]]. [[Ludwig Vanderhem]] befindet sich in einem Spiegel. 
[[Mirul]] in [[Ludwig Vanderhem]] genannt [[Mudwig]]. Wo Wildschwein?. Wo [[Yaku Purisqan]]? Wer ist in [[Mirul]]? Wer war im Wildschwein.
Ich kontrolliere [[Mirul]]s character in [[Ludwig Vanderhem]] Körper. [[Mirul]] Körper steht unter  suggestion und versucht den Boden zu erreichen [[Vincent]]
# Kamen vor
[[Lil Chunky]], [[Mirul]], [[Cali Bloom]], [[Neldon]], [[Ludwig Vanderhem]], [[Archimedes]], [[Yaku Purisqan]], [[Keiros]], [[Vincent]], [[Mudwig]]
# Quests
- [[Vincent]] ist ausgenockt. Wie bekommen wir die richtigen Körper Konfigurationen zurück [[Quest Körpertausch]]
- [[Vincent]] öffnet zumindest Teilweise ein Portal [[Quest Portalsiegel]]
# Zusammenfassung
Immer noch in [[Orte/Keiros Tempel]]. [[Neldon]] greift [[Vincent]] an und bricht damit [[Yaku Purisqan]] Konzentration auf suggestion. [[Vincent]] spring daraufhin ins Wasser und nimmt ein Portal. [[Yaku Purisqan]] springt auf der anderen Seite des pools ins Wasser. [[Vincent]] kommt in einen Raum mit drei menschlichen Leichen (vielleicht die Diebe die den Dungeon ausrauben wollten?) und einem Steinhaufen (Definitiv [[Big Chunky]]). Es riecht unangenehm. Im Raum sind drei grüne Siegel und ein Altar mit verdorbenem Essen.
[[Mudwig]] springt mit dem Topf, in bester Fluch der Karibik impression, ins Wasser.  Und versucht erneut mit [[Vincent]] eine Konversation anzufangen. [[Vincent]] fängt an ein Siegel zu zerstören. [[Neldon]] tröstet [[Lil Chunky]] und versucht mit ihm zusammen nach unten zu gehen. [[Yaku Purisqan]] castet Suggestion: "Unternehme keine weiteren Versuche, die Siegel oder irgendetwas anderes in diesem raum zu beschädigen". [[Mudwig]] verteilt essen. Wasser im Raum steigt weiter, alle versuchen den Raum zu verlassen außer [[Vincent]]. [[Mudwig]] kann den Raum nicht verlassen. Einige anderen können den Raum nicht verlassen. [[Vincent]] erzählt von ihrer Gruppe (drei Leichen) die versucht haben das portal zu öffnen und sind gescheitert.
[[Neldon]] zerstört eine Wand zu einem neuen Raum mit einem Siegel über ein Portal zum [[Feywild]]. [[Vincent]] ist nicht länger unter suggestion, attackieren  und zerstört teile des portalsiegels. Wird dann von [[Yaku Purisqan]], [[Neldon]] und [[Cali Bloom]] ausgenockt. [[Mudwig]] erkundet den rest des Tempels und adoptiert [[Lil Chunky]]. [[Lil Chunky]] vergräbt den Haarstein seines Papas in seinem Lavapool und will mitgenommen werden
[[Keiros]] scheint den Tempel gebaut zu haben. 
# Punkte für nächste Session
- [[Mudwig]] versucht gemäß [[Quest Körpertausch]] Ludwig seinen Körper zurückzugeben
- Wie instabil ist das Portal jetzt? Herrscht Gefahr?
# Notizen
Marti kämpft in dieser Session über große Teile ein 1v3. Es hat ihr trotzdem Spaß gemacht. Mehr darauf achten, dass alle an den Zug kommen.