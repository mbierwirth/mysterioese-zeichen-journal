---
tags:
  - recap
---
# Stand vorher
[[Session 41]]
# Kamen vor

# Quests
[[Quest Der Zauberberg]], [[Quest Der beste Schmied]],
# Zusammenfassung
[[Cali Bloom]] fragt Leute in der Stadt aus. Karl (ein Zwerg) arbeitet an einem Herzschrittmacher. Einige der Arbeiter verehren [[Altea]]. [[Oloramordin]] der Zauberberg ist nicht gut zu besteigen. Zwerge verehren den Berg schon lange. Die Gnome erst seit etwa 400 Jahren. Seit 150 Jahren sind die Gnome zu dem Berg gekommen um die Kristalle abzubauen und die Energie des Berges benutzen. Früher war [[Oloramordin]] eine Pilgerstädte für eine jetzt vergessene Relegion (sus, hat [[Failass]] was damit zu tun?). Der Gott ist eventuell verbannt worden. [[Ludwig Vanderhem]] schickt einen Brief an [[Phineas Fidelius Philemon]]:
Phineas, wir sind bei Mechenergy angekommen. Ich spüre eine ungute Energie die dieses Unternehmen umgibt, alles ist zu perfekt. Hatte die Qualitätskontrolle schon immer diesen Mantel der Geheimniskrämerei? Eine alte, ausgelöschte und vergessene Religion auf dessen Trümmern diese Firma aufgebaut zu sein scheint? Und diese aufgesetzt positive Einstellung die ein jeder einem hier entgegen bringt? Die Meinungsverschiedenheit bei der wir verblieben sind betrübt mich. Doch ich stehe zu meiner Haltung von Geschäftspartnern auf Augenhöhe. Gebührend Ludwig Vanderhem

[[Mirul]] verkauft some of her stuff. Sie versucht bei den Problemen der anderen zuzuhören. Sie schnappt Fetzen von einem gespräch zwischen zwei gnomen auf. Der eine scheint über jemanden zu erzählen der in der Qualitätskontrolle Mist gebaut zu haben, wird eventuell gefeuert. Sie verstummen als sie [[Mirul]] bemerken. [[Yaku Purisqan]] bemerkt in ihrer Rolle als Bademeisterin, dass zwei Gäste (die Drachengeborene mit dem Metallernen Auge) häufig den Berg anschauen. [[Valentina]] (ausgedachter Name) ist ein Aasimar, spirituelle Unterstützung für [[Mechenergy]] seit 8 Jahren. Sie sagt, dass Dinge macht besitzen wenn jemand an sie glaubt.
[[Neldon]] arbeitet weiter an seiner Hand, Karl und Karla werden zu Informations bzw Schildverstärkungsarbeiten abgestellt. Er lernt die anderen Schmiede besser kennen.
[[Cali Bloom]] und [[Ludwig Vanderhem]] spionieren die die Qualitätskontrolle aus. 9 Gnome und ein Mensch. Die zwei Gnome die [[Mirul]] gesehen hat tauchen da auf. Ebenfalls [[Ellys]] Vater. Der scheint sich aber nicht wirklich wichtig zu machen. 5 Gnome gehen tagsüber, 2 Gnome nachts, eine Gnomin immer mal wieder und Ellys dad ebenfalls. Der Code ist ein Zahlencode. 
[[Ludwig Vanderhem]] findet im Tempel einen Schatten von [[Yavita]]. [[Lauren]] taucht auf und bringt die Antwort von [[Phineas Fidelius Philemon]]. Er hat keine Ahnung von irgendwas, traut [[Orli]] das alles aber durchaus zu und hofft unsere Geschäftsbeziehung weiterzuführen.
Wir wollen zwei Gnome und einen Menschen austauschen. Einbrechen klappt supi, 5 humanoide erfolgreich unter Drogen gesetzt. Verkleidungen, verwandlungen unsichtbarkeit wird auf Tiere, Menschen, Feen und Genasi gecastet. [[Ludwig Vanderhem]] geht zuerst und versucht den Code zu erraten. Schafft es nicht und wird von den anderen vieren ins Kreuzverhör genommen. Er erzählt eine Lügen geschichte über ihn (Tobias) und seinen Freund (Juri) die sich gestritten hätten zum ertsne mal seit immer. Es klappt (Clutch 30). Und die anderen Wachen sind sehr mittleidig. Er kann den Code von zwei anderen eingaben erschließen. [[Cali Bloom]] und [[Yaku Purisqan]] schaffen es reinzugehen und nehmen einen unsichtbaren badger mit. Die Drachengeborene mit dem Mechana-Auge sieht den unsichtbaren Badger. Innen drin sind Tische für jeden Qualitätskontrolleur, einen Schreibtisch, eine kleine Schmiede. 
# Punkte für nächste Session
What the fuck is going on in the Qualitätskontrolle. Drachengeborener hat den unsichtbaren badger gesehen
# Notizen