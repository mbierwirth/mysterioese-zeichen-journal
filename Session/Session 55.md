---
tags:
  - recap
---
 # Stand vorher
[[Session 54]]
# Kamen vor
[[Sabrina]], [[Tiana]]
# Quests

# Zusammenfassung
Wir kommen zurück vom Fluss. Am abend setzten wir unseren großartigen Plan in die Tat um. [[Ludwig Vanderhem]] und [[Yaku Purisqan]], bleiben zurück, die anderen Teleportieren nach draußen und machen sich auf den Weg zum Antimagiefeld. Dort werfen sie allerlei magic items rein. Es kann nicht herausgefunden werden welche Gottheit für das Feld verantwortlich ist. [[Cali Bloom]] geht ins Feld mit einem Seil festgebunden. Sie verschwindet aus dem Sichtfeld. [[Mirul]] und [[Neldon]] versuchen sie zurückzuziehen, aber schaffen es nicht. [[Sabrina]] kommt angeritten, [[Neldon]] wird unsichtbar. Im Dorf machen [[Yaku Purisqan]] und [[Ludwig Vanderhem]] das Ablenkungsmanöver (aka laute Sexgeräusche) mit einer großartigen Performance. [[Cali Bloom]] hat keinerlei erinnerungen an was in dem Feld passiert. [[Mirul]] conned [[Sabrina]] mit einer Story über eine Orknachwanderung + unangenehme Situation zuhause. [[Sabrina]] ist interessiert. Alle machen sich auf den Weg zurück. [[Cali Bloom]] wird aufgegabelt von Zwerg. Zuhause geben Yaku und [[Ludwig Vanderhem]] erneut eine fantastische Performance ab. [[Sabrina]] verbringt die Rest der Nacht bei uns zu Hause und bekommt einen Schmiedevortrag von [[Neldon]]. [[Holly]] und [[Zion]], sind schwer verwirrt und haben scheinbar nicht zugehört. 
Am nächsten morgen, machen wir uns auf den Weg zum Strand und verabschieden uns da von [[Sabrina]], sie scheint glücklich zu sein uns loszuwerden. Wir treffen Drachengeborene Capitano [[Tiana]]. Ihr wurde erzählt uns besonders im Auge zu halten. Wir haben eine Koje ohne Bullauge. [[Mirul]] bringt uns bei ihre Runen zu invoken. [[Yaku Purisqan]], [[Cali Bloom]] und [[Holly]] haben eine Übernachtungsparty. [[Holly]] mag ihren alten Namen nicht mehr. [[Neldon]] und [[Ludwig Vanderhem]] führen ein wichtiges Businessmeeting über grüne rote Zahlen, Workflow optimierungen und dem richtigen mindset. [[Neldon]] ist auf der Quest alle Schmiedetechniken der Welt zu erforschen. [[Mirul]] führt eine Forschungsstudie mit [[Zion]] dem Vampir durch:
	knoblauch? nein 
	sonnenlicht? ja 
	einladung? ja 
	spiegelreflexion? ja 
	gefühle? ja 
	änderung? sehr starke gefühle, Hunger, später balance biss betäubt liebe: ja
[[Mirul]] ist immer noch stark davon überzeugt, dass mit [[Ludwig Vanderhem]] etwas nicht stimmt, selbst wenn er kein Vampir sein sollte.
[[Mirul]] hat wieder albträume. Steht alleine im Wald. Ihre mutter hat sie verlassen. Ihr ist kalt. Sie ist blutig. Sie rennt im Wald herum und findet eine große weiße Schleiereule. Das erste mal wieder licht. "Wenn dich alle verlassen habe, wohin gehst du. Es werden mich nicht alle verlassen. Sie haben es getan, sie werden es tun. Nein..., nein  warum sagst du so etwas. Ich versuche dich vorzubereiten. Hör auf sowas zu sagen. Wohin wirst du gehen Mirul". Es wird dunkel. [[Mirul]] läuft weg, findet aber niemanden. Stopelt über etwas oder wen. Bart, braids, es ist [[Malon]]. "Es tut mir leid Mirul. Ich konnte es nicht aufhalten, ich konnte dich nicht stark genug machen." [[Malon]] stirbt in dem Traum.
# Punkte für nächste Session

# Notizen