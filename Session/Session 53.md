---
tags:
  - recap
---
# Stand vorher
[[Session 52]]
# Kamen vor
[[Sabrina]]
# Quests
[[Quest Eine Gottheit am Telefon]], [[Quest Die Insel die nicht existiert]]
# Zusammenfassung
[[Mirul]] wacht auf wegen Albträume involvierend ihre Mutter und Bruder. Sie sieht ihre Mutter die sagt, dass sie sie immer verlassen wollte und sie nie geliebt hat. Wir reden das erste Mal mit Quest Eine Gottheit am Telefon. Eine Präsenz scheint aus dem Diadem hervorzugehen und den Raum beengend klein zu machen. Aus unseren Fragen geht hervor, das Quest Eine Gottheit am Telefon weder auf unserer noch auf der seite der insel steht. 
Wir machen uns auf auf unsere Bergwanderung mit [[Sabrina]]. [[Mirul]] stellt fest, dass die Knöpfe im Ohr das Zeichen von [[Mechenergy]] tragen, und somit eventuell abgehört werden können. [[Sabrina]] wird auf das Teil aumerksam und [[Mirul]] schluckt es herunter. Möglicherweise gibt es auf der Insel eine Schmiede im Verbotenen Teil. Wir finden raus, dass [[Sabrina]] eine begabte Spellcasterin ist. Sie hat ihren eigenen Kühlraum gewirkt, und ihr Schlafzimmer ist Schallisoliert. 
In [[Salost]] dürfen viele der Mechana gegenstände nicht importiert werden, da sie mit verbotenen Göttern in verbindung gebracht werden und ihnen keine Qualitätsstandards unterliegen.
Zwischen [[Holly]] und [[Zion]] knistert es gewaltig. Zwei weitere Knöpfe im Ohr werden ausgeschalten. 
[[Mirul]] versucht die Rune von [[Dahia]] zu envoken. Diese scheinen energy von [[Mirul]] aufzusaugen. Plötzlich spricht [[Keiros]] in [[Mirul]]s Kopf und bittet sie es nicht zu tun. [[Mirul]] lässt in Folge dessen von weiterem Envoking ab. Plötzlich gehen sirenen im verbotenen Teil los. [[Sabrina]] sagt uns das wir auf keinen Fall hinterher sollen. 
[[Cali Bloom]] fliegt als Kunschafter aus. Auf halben weg trifft sie auf eine Antimagiemauer. Sie sieht rotes höllenfarbiges Licht von Gebäude kommen. Dann fliegt sie zurück.
[[Ludwig Vanderhem]], [[Abraxas]] und [[Mirul]] gehen ebenfalls zum Gebäude. Das Magiefeld scheint einer bestimmten Gottheit gewidmet und alle andere Magie zu unterdrücken. Wir gehen rein. Syke doch nicht, weil wir uns nicht trauen.
Die anderen durchsuchen derweil die Häuser von [[Sabrina]], [[Tibald]] und die Kirche. Die Insel wird beliefert mit essen einer Menge Arcana stuff. Außerdem finden wir raus, dass die meisten gut mit magie oder religion sind.
Unter Sabrinahs bett finden wir eine Priesterrobe.
Yaku führt deep talk mit [[Holly]]. Sie offenbart, dass [[Zion]] gefragt hat ob er ihr Blut trinken dürfte. Sie scheint nicht abgeneigt.
# Punkte für nächste Session
Sind die Sirenen direkte Konsequenz des Runen invokens gewesen?
# Notizen