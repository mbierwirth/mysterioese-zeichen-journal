---
tags: recap
---
# Stand vorher
[[Session 36]]
# Kamen vor
[[Lil Chunky]], [[Fabia Sandig]], [[Sanja]], [[Iandir]], [[Ludwig Vanderhem]], [[Neldon]], [[Mirul]], [[Cali Bloom]]
# Quests
[[Quest Portalsiegel]], [[Quest Ludwigs Mommyissues]], [[Quest Eine Statur von Mirul]], [[Quests/Quest Der beste Schmied]], [[Quest V for Vesta]]
# Zusammenfassung
Shopping Episode:
[[Ludwig Vanderhem]]: [[Drachenleder Hut]], Taschenmesser, Seidenseil, [[Boots of Mechanakind]], Healing Potion, Harte Thieves Tools, Cooles Disguise Kit
[[Mirul]]: Feuerlöscher, Hut mit Ente, Plate Armor (gebaut von [[Neldon]]), [[Belt of Giant strength]], unendliche Gewürze, [[Deck of Illusion]], Spitzhacke, Reagenzgläser
[[Neldon]]: Plate Armor (gebaut von [[Neldon]])
[[Yaku Purisqan]]: Taubenhut, [[Feywild Shard]], [[Cloth of many Clothes]], [[Emotion Paint]]
[[Cali Bloom]]: Huhnhut

[[Mirul]] verkauft mit [[Ludwig Vanderhem]] Hilfe [[Mrs Theodora]] verziehrten Hammer. [[Ludwig Vanderhem]] lässt sich in einem Hut Laden ausstatten und kauft einen goldenen Drachen Leder Hut. [[Mirul]] und [[Yaku Purisqan]] klauen einen Stein aus der Stadtmauer und werden dabei von den Wachen erwischt. Der Stein wird trotzdem mitgenommen und später [[Lil Chunky]] übergeben. [[Neldon]] schmiedet Rüstungen. [[Cali Bloom]] trifft [[Iandir]] und [[Sanja]]. Beide sehen so aus als hätten sie wenig bis gar nicht geschlafen. Sie erzählen, dass das vor [[Ludritz]] geöffnete Portal nicht zum [[Feywild]] führt. Die [[Feywild]] aktivität in der Region scheint abgenommen zu haben seitdem. Das Portal neben [[Martha]] Haus ist geschlossen. [[Yaku Purisqan]] findet einen Zettel unterzeichnet von [[V]]. [[Ludwig Vanderhem]] trifft sich mit [[Fabia Sandig]]. [[Fabia Sandig]] erzählt von der Zeit vor [[Emilia Vanderhem]] verschwinden. Damals scheint sie mit [[Erkan]], [[Tenevra]], einer Orkfrau und einem Elfen Mann in den Westen gezogen zu sein um dort für [[Keiros]] einen Auftrag auszuführen. Von diesem Auftrag scheint nur [[Emilia Vanderhem]] schwer krank zurückgekommen zu sein. Ihre persönlichen Gegenstände sind dabei unauffindbar gewesen. Alle Ärzte die sie im Anschluss untersucht haben sind verschwunden oder gestorben. Sie hat [[Fabia Sandig]] weitestgehend aus der Sache heraushalten wollen. Darüber war [[Fabia Sandig]] damals sauer und ist es heute immer noch ein wenig.
Letzte Notiz von [[Emilia Vanderhem]] an [[Fabia Sandig]] die [[Fabia Sandig]] bei ihr fand, [[Emilia Vanderhem]] war nicht mehr in der Lage etwas darüber zu sagen und starrte sie nur blank an. Behalte sie im Auge aber lass dich nicht erwischen. Halte dich von [[Henrik]] fern. Wir wurden verraten, sei vorsichtig gegenüber Fremden.
Letzter Brief: 
Meine liebe Fabia,
meine Mission schreitet gut voran. Ich weiß dass du unzufrieden bist da ich dich nicht in meine Angelegenheiten involviere, aber ich bin froher denn je. Mehr und mehr merke ich in was für eine gefährliche Sache Erkan und ich uns verwickelt haben. Falls ich zurückkomme verspreche ich dir mehr zu erzählen und dich endlich aufzuklären. Doch in den letzten Tagen zweifele ich oft und falls ich es nicht schaffen sollte bitte ich dich: Versuche nicht auf eigene Faust Nachforschungen anzustellen. Halte dich fern von meiner Familie. Denke über einen Umzug aus Bremwede nach. Achte mehr als je zuvor auf deine Sicherheit. Umso weniger du weißt, umso weniger du mit mir in Verbindung gebracht wirst umso besser.
Ich hoffe du kannst mir vertrauen dass all dies nur zu deinem Besten ist.
In Sorge aber voller Hoffnung,
Emilia
[[Emilia Vanderhem]] scheint keiner Krankheit erlegen zu sein, sondern etwas anderem. Eine Spur führt in den Westen. [[Fabia Sandig]] behauptet sich immer um [[Ludwig Vanderhem]] und [[Cecilia Vanderhem]] gekümmert zu haben. Insbesondere [[Cecilia Vanderhem]] scheint sie zu mögen. Nach [[Emilia Vanderhem]] Tod hat sie eigene Nachforschungen angestellt. Daraufhin wurde häufig bei ihr eingebrochen, weswegen sie schließlich die Suche aufgegeben hat. Sie stieß bei ihren Nachforschungen auf viel [[Feywild]] Aktivität im Westen und noch einer anderen Signatur. Sie hatte eigentlich versprochen [[Ludwig Vanderhem]] nie etwas darüber zu erzählen. 
[[Neldon]] trifft die eine Personalerin [[Hattie]] von [[Mechenergy]] . Er einigt sich mit ihr auf eine Woche Probearbeiten nach dem Abschluss der Wahlen. Dies findet im Westen des Landes statt. [[Cali Bloom]] und [[Ludwig Vanderhem]] folgen [[Hattie]], verschmelzen neben ihrem Haus mit den Schatten und steigen schließlich, auf der Suche nach Dokumenten die [[Neldon]] beinhalten, bei ihr ein. Stehen zunächst im Wohnzimmer und finden eine Schundroman sammlung. [[Ludwig Vanderhem]] geht in ihr Schlafzimmer und greift alle Notizen auf ihrem Schreibtisch. Darunter Dokumente über [[Neldon]], [[Molly]] und zwei andere Gnome die von [[Mechenergy]] angeworben werden sollen. Außerdem einen Schundroman an dem [[Hattie]] gerade schreibt. [[Ludwig Vanderhem]] schleicht sich zurück in ihr Wohnzimmer, schreibt mit schwarz an die Wand "Ihr seid nicht die einzigen die Drohungen aussprechen können. PS: eindimensionale Charaktere, langweilige Dialoge, und ein grottiges Pacing". Dann zündet er Teile der briefe und des Romans an und lässt diese in der Wohnung zurück.
[[Lil Chunky]] genießt seine Wohnung bei [[Neldons Chef]]. Er wohnt im Ofen und ist glühend heiß. Wir machen mit ihm einen Ausflug in das Spa Schwimmbad. Vorher geben wir ihm den Stein der Stadtmauer zu essen. Er freut sich. Im Schwimmbad isst er 97 Massagesteine (9 gold 7 silber), reinigt beim Mudding das Bad, gewinnt eine Wasserschlacht mit [[Neldon]], nimmt erfolgreich an einer Schlammschlacht mit [[Yaku Purisqan]], [[Ludwig Vanderhem]] und [[Cali Bloom]] teil und schläft auf den Saunasteinen ein. Am Abend macht er noch mit [[Mirul]] einen Spaziergang um sein Liebling Gürteltier zu besuchen. Selbst das Spa scheint [[Mirul]]s Gelenkschmerzen nicht zu heilen. 
[[Mirul]] und [[Ludwig Vanderhem]] haben immer noch Kopfschmerzen.
# Punkte für nächste Session
Abholen des Drachenleder hutes (Verweis auf den Drachentöter Background)
# Notizen