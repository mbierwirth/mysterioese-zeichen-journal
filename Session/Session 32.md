---
tags: recap 
---
# Stand vorher
[[Session 31]]
# Kamen vor
[[Markus]], [[Cali Bloom]], [[Yaku Purisqan]], [[Mirul]], [[Ludwig Vanderhem]], [[Neldon]], [[Sanja]], [[Iandir]], [[Failass]], 
# Quests
[[Quest Inseln vor Ludritz]], [[Quest Körpertausch]], [[Quest Failass Grudge]], [[Quest Ludwigs Mommyissues]]
# Zusammenfassung
Die Party ist auf dem Boot. Der Sturm schwillt an. Die strömung treibt das boot aus der bucht, der wind in richtung osten. [[Yaku Purisqan]] und [[Ludwig Vanderhem]] holen die Segel ein. [[Cali Bloom]] wird schlecht. Der Plan ist um die Insel in den Windschatten zu fahren. [[Neldon]] und [[Mirul]] schaffen es nicht das Boot zu lenken.  [[Cali Bloom]] möchte sich umschauen aber sich nicht wie ein Drache von [[Ludwig Vanderhem]] in den Wind gehalten werden. [[Ludwig Vanderhem]] fragt [[Failass]] wie man ein Segelboot in einem sturm lenkt. [[Failass]] schickt einen Kraken mit roten Augen der das Boot angenehm um die Insel in Sicherheit schaukelt. Die Party erklimmt die Klippe zur Insel um zu Kochen. [[Yaku Purisqan]] hat trotz Kletterklauen Probleme beim erklimmen und lässt sich erst von [[Ludwig Vanderhem]] und [[Mirul]] helfen. [[Mirul]] kocht, essen ist geil. Die Inseln sehen so aus, als wären sie aus einem Stein geformt worden der umgefallen und zersplittert ist. Die Zwischenräume zwischen den inseln sind abgespühlt.
[[Mirul]] pingt [[Fabia Sandig]] an um [[Vincent]] einen neuen Körper zu verschaffen. [[Fabia Sandig]] sagt das wir Hilfe bei ihr, [[Aurelius]], [[Ganimedes]] oder in [[Bremwede]] bekommen können. [[Ludwig Vanderhem]] fragt [[Failass]] ob er den Kraken veranlasst das Boot nach [[Ludritz]] zu bringen. Er verlangt dafür Information über [[Keiros]] Verbindungen zu anderen [[Feywild]] Göttern. Nach Absprache mit [[Neldon]] wird das Geschäft abgelehnt. Man will sich nicht abhängig machen. 
Wir fahren nach Ende des Sturmes nach [[Ludritz]] zu [[Fabia Sandig]]. [[Selene]] macht die Tür auf. Sie nennt  die drei möglichen Optionen [[Aurelius]], [[Rachelia]] und [[Ganimedes]]. [[Neldon]] will eigentlich nicht gehen wird aber von den andere überzeugt. Wir verlassen [[Fabia Sandig]] durch ein Portal direkt zu [[Ganimedes]] Turm. [[Ludwig Vanderhem]] beginnt sich während des Gesprächs an [[Fabia Sandig]] zu erinnern. Sie war die Schülerin von [[Emilia Vanderhem]]. Dann fragt er sie bezüglich [[Emilia Vanderhem]] ohne das die anderen etwas mitbekommen
# Punkte für nächste Session

# Notizen