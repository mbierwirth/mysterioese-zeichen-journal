---
tags: recap
---
# Stand vorher
[[Session 30]]
Kurz:
[[Vincent]] machte Teil des Portalsiegels kaputt, jetzt Ohnmächtig. [[Lil Chunky]] dad ist tod, vermutlich wird er adoptiert. [[Vincent]] und [[Mudwig]] sind immer noch nicht in ihren Körpern. Was machen [[Cali Bloom]] freunde. [[Yaku Purisqan]] ist immer noch unklar
# Kamen vor
[[Mudwig]], [[Ludwig Vanderhem]], [[Cali Bloom]], [[Mirul]], [[Vincent]], [[Neldon]], [[Yaku Purisqan]], [[Lil Chunky]], [[Archimedes]], [[T.A.]], [[Failass]], [[Sanja]], [[Iandir]]
# Quests
[[Quest Körpertausch]], [[Quest Portalsiegel]], [[Quest Failass Grudge]]
# Zusammenfassung
[[Yaku Purisqan]] ist mega sus. [[Neldon]] und [[Cali Bloom]] können unbeschadet den Siegelraum verlassen. [[Mudwig]] geht in den Raum und [[Ludwig Vanderhem]] kann wieder seinen eigenen Körper zurückkehren. [[Vincent]] wird in den Raum geschubst. Keine Konsequenzen. [[Lil Chunky]] nimmt ein Lavabad. [[Vincent]] aka [[Vincent]] deckt seine Vergangenheit und beschließt zumindest zum Teil der Party zu vertrauen. Wurde von [[T.A.]] beauftragt. 
Abmachung mit [[Vincent]]: Sie geht freiwillig in den Raum und failed ihren safe. [[Mirul]] wieder in [[Mirul]], dann geht [[Cali Bloom]] Badger rein und nimmt [[Vincent]] Geist. Dieser wird dann getötet damit [[Vincent]] in [[Miruls Bild]] Platz nehmen kann und später einen Körper bekommt.
[[Vincent]] geht rein, [[Mirul]] scheint wieder herauszukommen. Findet es aber nicht fair den Jungen in [[Miruls Bild]] zu überschreiben.[[Ludwig Vanderhem]] versucht mit [[Lil Chunky]] rutschen zu gehen (und dabei die Höhle zu verlassen) aber [[Yaku Purisqan]] versucht das zu verhindern und wird in einem Schlag von [[Cali Bloom]] ausgenockt. [[Yaku Purisqan]] versucht den Tempel zu beschützen. [[Yaku Purisqan]] wird in den Raum gestoßen und kommt wieder zurück in Ihren Körper. Ihr ist unklar wer genau in Ihrem Körper war. Wir versuchen über die Rutsche wieder nach oben zu gelangen zusammen mit [[Vincent]] in Badger Körper. Funktioniert. Wir treffen [[Iandir]] oben und [[Sanja]] auf dem Boot. [[Ludwig Vanderhem]] wird von einer ominösen Stimme in seinem Kopf bezüglich [[Keiros]] gefragt. Er will auf starke nachfrage [[Failass]] oder Gerechtigkeit genannt werden und behauptet von [[Keiros]] zerstört worden zu sein. [[Keiros]] habe Familien zerrissen und Freundschaften zerstört haben um dahin zu kommen wo er jetzt ist. Auf dem Weg bewegen sich Sturmwolken schnell auf uns zu
# Punkte für nächste Session
Sturm auf dem Boot
[[Vincent]] ist ein Badger
# Notizen
Reihenfolge:
wildschwein, [[mirul]], [[mirul]], yaku, ludwig, ludwig

Aktuell:
Ludwig in Ludwig
[[Mirul]] im spiegel

#todo abschluss [[Quest Körpertausch]]