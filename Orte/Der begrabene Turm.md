---
tags: ort
---
# Name
Der begrabene Turm
# Karte von Der begrabene Turm
Liegt im Moor bei [[Die Verlassenen Länder]]
![[Der begrabene Turm Keller.png]]
![[Der begrabene Turm Erdgeschoss.png]]![[Der begrabene Turm 1 Stock.png]]
# Beschreibung des Ortes
Ein Höhlenkomplex führt zu einem im Moor begrabenen Turm. Durch den Keller erreicht man den ersten und zweiten Stock. Von drinnen sieht es so aus als würde man auf eine bunte Blumenwiese schauen. Es scheint auch ein unabhängiges Wetter zu geben. Der Turm gehörte einst [[Lydia]]. Bevor sie und [[Ganimedes]] aus der Region verdrängt wurden.
# Relevante Quest
[[Quest ein Schatz im Sumpf]], [[Quest Lydias Tagebuch]]
# Interessantes
Die Gruppe findet den Turm verlassen, zerstört und gespickt mit Skelletten. Im doppelten Boden einer Komode befindet sich [[Nekronomicon (Lydias Tagebuch)]].