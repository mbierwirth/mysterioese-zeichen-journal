---
tags: ort, land
---
# Karte
![[WorldMap.webp]]
# Städte
- [[Bremwede]]
- [[Ludritz]]
- [[Königswald]]
# Geschichte
Waringen ist ein ehemaliges Königreich dessen Kronfamilie meist entweder selber magisch begabt waren oder von mächtigen Magieren, meist einem Gott zugehörig, unterstützt wurde. Die meisten Herrscher beriefen sich dabei auf [[Sarastro]].
Seit einigen Jahrzehnten schwindet die Magie aber langsam aus dem Land, der Ursprung ist unklar. Gelehrte vermuten dass es einen allgemeine Verringerung gab da die letzte Generation der Magier versäumt haben weitere auszubilden, sodass wenig Leute die Magie nutzten was wiederum zu einem weiteren verschwinden führte. Es werden auch nur noch wenig verschiedene Götter verehrt da die Macht der alten Götter ebenfalls zu schwinden scheint.
Den Platz der Magie (Arcana) haben nun die von den Gnomen schon lange hergestellten Mechanagegenstände eingenommen, hier wird Magie mit Technik verbunden um die benötigte Magie zu minimieren. Dies ist mittlerweile auch bei den Menschen, Orks, Halblingen und vielen anderen Rassen angekommen. Drachengeborene sind selten zu finden, dafür gibt es immer mehr Roboter mit eigenem Bewusstsein und vor allem Cyborgs.
Viele der Menschen sind mittlerweile überzeugt dass die Mechana von ihnen erfunden wurde, trotzdem die Gnome immernoch technologisch am Fortgeschrittensten sind.
Elfen sind immernoch nicht überzeugt weder von Arcana noch Mechana, die einzige Art mit solcher Macht natürlich in Verbindung zu treten ist für sie das Druidentum, während die Zwerge ihr eigenes Handwerk gegenüber Mechana bevorzugen.
Dies hat auch eine neue Göttin hervorgenbracht, die Göttin der Technik und des Fortschritts, [[Altea]].
Durch das Schwinden der Macht der Götter und gleichzeitiges Starten des Mechana Zeitalters schwand die Macht der Königsfamilie, sie sind zwar noch offiziell die Herrscher des Königreichs, halten aber keine tatsächliche Macht und haben keine große Armee.
Stattdessen sind die größeren Städte regiert durch verschiedenste Systeme, durch eigene Fürsten, Räten der reichsten Kaufmänner oder auch Demokratien.
Die Menschen ziehen immer mehr in die großen Städte da durch die weniger werdenden Magier die Dörfer sehr schutzlos sind und mehr und mehr von wilden und sogar dämonischen Kreaturen überfallen werden. Viele von ihnen sind noch misstrauisch gegenüber Mechana, fühlen sich aber auch im Stich gelassen von den Magiern des Landes.

# Geographie
[[Waringen]] wird im Norden und Osten durch das Meer begrenzt, im Westen durch einen großen See (dahinter liegt [[Ganadia]]) und im Süden grenzt es ans [[Sinderreich]]. Die diplomatischen Beziehungen sind seid Machtabnahmd der Könige geschwächt da beide Länder noch tatsächliche Monarchien sind und grade mit [[Ganadia]] durchaus angespannt. In der Mitte des Landes (um [[Bremwede]]) und bis in den Süden ist vor allem Wald, an der Küste im Norden hohe Klippen, im Osten vor allem Sandstrand. Im Westen findet man dann Berge bis an den großen See.

# Religion
[[Sarastro]] stammt aus dem einstmals größten Pantheon in dem er als Gott des Mondes und der Weisheit verehrt wurde, schon lange vor dem Schwinden der Magie hatten seine Verehrer aber begonnen ihn als einzig wahren Gott zu sehen. Seine Mitgötter sind aus dem Gedächtnis der Welt, mit Ausnahme von einigen Gelehrten in dem Gebiet, größtenteils verschwunden.
Auch heute berufen sich seine Verehrer auf seine Weisheit, Güte und Stärke. Sein Symbol ist eine gespiegelte Mondsichel auf hellblauem Grund.
[[Altea]] wurde zuvor unter anderem Namen als Göttin des Handwerks und praktischem Wissen verehrt, stammt aber aus einem anderen Pantheon als [[Sarastro]]. Sie hat nun die Rolle als Göttin für Mechana und Fortschritt übernommen, von den Gnomen wird sie allerdings nicht verehrt. Sie wird vor allem für ihren Weitblick und ihr Erfindungsreichtum verehrt. Ihr Symbol ist eine aufgehende Sonne auf lila Grund, wobei die untere Hälfte oft durch eine Mechana Quelle in grauem Grund ersetzt wird.
Andere Gottheiten werden vor allem von Bewohnern kleinerer Dörfer verehrt, auch die Elfen und Zwerge verehren noch eigene Götter.
Zudem gibt es den in vielen Städten präsenten, aber neuen und bei der allgemeinen Bevölkerung unbekannten [[Tempel der ewigen Liebe]]. Sein Symbol ist ein weißes Herz in goldener Fassung.
[[Yavita]] ist die Göttin des Todes. Besonders verehrt in [[Die Verlassenen Länder]] 