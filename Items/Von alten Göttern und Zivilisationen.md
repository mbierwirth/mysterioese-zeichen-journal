---
tags: item
---
# Name
Von alten Göttern und Zivilisationen
# Effekt
Enthält Informationen zu einer Reihe von alten Göttern unter anderem zu [[Nenith]], [[Beirdess]] und [[Andiliam]]
Früher waren [[Waringen]] und [[Feywild]] mehr Verbunden.
Scheint Informationen über das Öffnen und Schließen von [[Feywild]] zu beinhalten
# Aktueller Besitzer
[[Ludwig Vanderhem]]
# Ehemalige Besitzer 
[[Emilia Vanderhem]]
# Notizen
Buch lag in [[Emilia Vanderhem]] Zimmer nach ihrem Tod. Es scheint sich in keiner Weise von einer normalen Ausgabe die aus der [[Bücherei]] ausgeliehen wurde zu unterscheiden