---
tags: item
---
# Name
Miruls Bild
# Effekt
Sobald das Bild das erste Mal von einer Person begutachtet wird, muss ein wisdom save gemacht werden, ansonsten fürchtet man sich vor dem Bild und denkt es sei verflucht.
Das Bild beherbergt eine Seele die darin aufbewahrt werden kann. Sobald die vordere Seite des Bildes mitbekommt wie jemand stirbt, wird die Seele dieser Person in dem Bild gespeichert. Falls eine andere Seele sich zeitgleich im Bild befindet wird diese überschrieben. 
Es ist möglich telepathisch mit der Seele im Bild zu kommunizieren.
## Aktuelle Seele 
[[Junge im Bild]]
## Ehemalige Seelen

# Aktueller Besitzer
[[Mirul]]
# Ehemalige Besitzer 
[[Mrs Theodora]]
# Notizen