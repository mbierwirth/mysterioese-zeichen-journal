---
tags: item
---
# Name
Nekronomicon (Lydias Tagebuch)
# Link

# Effekt
Das Buch hält Informationen über dunkle Zauber, Rituale und Beschwörungen, die in der Lage sind großes Chaos anzurichten. Das lesen des Buches führt zu immensen Kopfschmerzen und moralischen Bedenken. Es steht im starken Kontakt mit [[Yavita]]
# Aktueller Besitzer
[[Ganimedes]]
# Ehemalige Besitzer 
[[Lydia]], [[Ludwig Vanderhem]], [[Neldon]], [[Mirul]]
# Notizen
Dieser Effekt des Buches wurde von [[Mirul]] und [[Ludwig Vanderhem]] erfahren