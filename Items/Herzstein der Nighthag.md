---
tags: item
---
# Name
Herzstein der Nighthag
# Aussehen
# Link

# Effekt
Der Herzstein kann zum heilen aller möglicher Flüche und Krankheiten benutzt werden. Wenn potenziert wird sein Effekt noch größer. Dazu muss er zuvor aus den verwelkenden Überresten einer Nighthag gewonnen werden. 
# Aktueller Besitzer

# Ehemalige Besitzer 
[[Ludwig Vanderhem]], [[Nighthag]]
# Notizen
Der von [[Neldon]] und [[Ludwig Vanderhem]] geernteter Herzstein wurde zur Heilung von [[Mirul]] in [[Quest Eine Statur von Mirul]] verwendet. 