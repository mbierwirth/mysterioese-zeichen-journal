---
tags: item
---
# Name
Bones of the Dying One, Eduard Fred ([[Mirul]] und [[Ludwig Vanderhem]])
# Link

# Effekt
Weissagung
# Aktueller Besitzer
[[Mirul]]
# Ehemalige Besitzer 
[[The Dying One]], [[Ludwig Vanderhem]]
# Notizen
Das Centerpiece bildet der Schädel des [[The Dying One]]. Ludwig verwendet zunächst aus Spaß die Knochen als Wahrsage tool. Überraschenderweise scheint es zu funktionieren. Bei der Benutzung scheint [[Ludwig Vanderhem]] die Kraft von [[Yavita]] zu spüren. 
[[Mirul]] benutzt den Kopf um damit Bauchzurednern.