---
tags: item
---
# Name
Sending Stone Fabia
# Link
http://dnd5e.wikidot.com/wondrous-items:sending-stones
# Effekt
25 Worte + Antwort pro Tag pro Seite kann über beliebige Distanz kommuniziert werden
# Aktueller Besitzer
[[Mirul]], [[Fabia Sandig]]
# Ehemalige Besitzer 

# Notizen
[[Mirul]] erhielt den Stein da sie [[Fabia Sandig]] bei etwas half und sie sich dankbar zeigte.