---
tags: item, magical
---
# Name
Amulet Of Proof Against Detection And Location
# Link
https://roll20.net/compendium/dnd5e/Amulet%20of%20Proof%20against%20Detection%20and%20Location#content
# Effekt
Während dieses Amulett getragen wird, ist der Träger geschützt vor jeglicher divinations Magie. Der Träger kann nicht durch solche Magie getargetet werden oder durch Wahrnehmungsensoren erfasst werden.
# Aktueller Besitzer
[[Ludwig Vanderhem]]
# Ehemalige Besitzer 
[[Emilia Vanderhem]]
# Notizen
Bedeutet Ludwig viel, da es eines der einzigen Erbstücke von [[Emilia Vanderhem]] darstellt