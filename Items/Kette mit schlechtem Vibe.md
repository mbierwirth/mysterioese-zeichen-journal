---
tags: item
---
# Name
Kette mit schlechtem Vibe
# Aussehen
In die Kette ist ein schwarzer Stein eingelassen 
# Link

# Effekt
Die Kette stärkt den Körper und klart den Geist des Nutzers. Die Kette ermöglicht es dem Nutzer einmal pro Tag Cause Wounds zu casten. Das Tragen der Kette scheint süchtig zu machen. Durch attunment kann noch größere Macht erlangt werden. Die Kette ist verflucht und ernährt sich von der Lebensenergie des Trägers. [[Ludwig Vanderhem]] opferte sie an einem Altar der [[Yavita]]
# Aktueller Besitzer
[[Yavita]]
# Ehemalige Besitzer 
[[Ludwig Vanderhem]]
# Notizen