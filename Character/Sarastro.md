---
tags:
  - character
  - npc
---
# Name
Sarastro
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Sarastro

# Gossip über Sarastro

# Meinung von Ludwig über Sarastro