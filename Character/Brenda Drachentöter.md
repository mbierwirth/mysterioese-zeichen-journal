---
tags:
  - character
  - npc
---
# Name
Brenda Drachentöter
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Brenda Drachentöter

# Gossip über Brenda Drachentöter

# Meinung von Ludwig über Brenda Drachentöter