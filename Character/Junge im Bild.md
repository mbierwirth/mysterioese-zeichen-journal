---
tags:
  - character
  - npc
---
# Name
Junge im Bild
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Junge im Bild

# Gossip über Junge im Bild

# Meinung von Ludwig über Junge im Bild
