---
tags:
  - character
  - npc
---
# Name
Vater Vanderhem
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Vater Vanderhem

# Gossip über Vater Vanderhem

# Meinung von Ludwig über Vater Vanderhem