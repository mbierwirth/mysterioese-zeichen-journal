---
tags:
  - character
  - npc
---
# Name
Mia
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Mia

# Gossip über Mia

# Meinung von Ludwig über Mia