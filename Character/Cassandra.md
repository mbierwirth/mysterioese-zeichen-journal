---
tags:
  - character
  - npc
---
# Name
Cassandra
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Cassandra

# Gossip über Cassandra

# Meinung von Ludwig über Cassandra