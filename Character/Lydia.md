---
tags: character, npc
---
# Name
Lydia
# Aussehen

# Heimat
[[Die Verlassenen Länder]], [[Der begrabene Turm]]
# Beruf
Anbeterin der Göttin [[Yavita]], Meisterin von [[Ganimedes]]
# Personen
## Freunde
[[Ganimedes]], [[Yavita]]
## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Errichtete den [[Der begrabene Turm]] in dem auch [[Quest Lydias Tagebuch]] versteckt war. Sie ist tot.
# Meinung von Lydia

# Gossip über Lydia

# Meinung von Ludwig über Lydia