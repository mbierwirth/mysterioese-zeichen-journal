---
tags: character, npc
---
# Name 
Kynan Tansen
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
Leiter vom [[Tansen Empire]] 
# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Adelhold]]
- [[Carl Erich]]
- [[Peter Patrick]]
- [[Cerit]]
### Professoren
- [[Alexandra Hauser]]
- [[Benjamin Klapper]]
- [[Dominik Nobel]]
## Freunde
- Mit [[Anne Cathryn]] zusammen
## Feinde

# Verbindungen
[[Tansen Empire]] 
# Interessen

# Gossip

# Vergangenheit
## Vor der Campaign 
Ist mit [[Ludwig Vanderhem]], [[Adelhold]], [[Carl Erich]], [[Peter Patrick]], [[Anne Cathryn]] und [[Cerit]] zur [[Kaufmannsschule]] gegangen.
## Seit der Campaign

# Motivation

# Meinung von 

# Ludwigs Meinung
Scheiß Streber