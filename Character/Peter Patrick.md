---
tags: character, npc
---
# Name
Peter Patrick
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
Leiter des Unternehmens [[Living Metal]]
# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Adelhold]]
- [[Kynan Tansen]]
- [[Carl Erich]]
- [[Anne Cathryn]]
- [[Cerit]]
### Professoren
- [[Alexandra Hauser]]
- [[Benjamin Klapper]]
- [[Dominik Nobel]]
## Freunde

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Ist mit [[Ludwig Vanderhem]], [[Kynan Tansen]], [[Adelhold]], [[Carl Erich]], [[Anne Cathryn]] und [[Cerit]] zur [[Kaufmannsschule]] gegangen.
Hat das Mechana Unternehmen [[Living Metal]] von seinem Vater geerbt. Dies war schon immer ein Selbstläufer
## Seit Beginn der Campaign

# Meinung von Peter Patrick

# Gossip über Peter Patrick
Geht gerne Trinken
# Meinung von Ludwig über Peter Patrick
Großkotz, arrogant, wird alles in die Wiege gelegt. Behauptet von sich, dass er "erfolgreicher als alle anderen ist".