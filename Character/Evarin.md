---
tags:
  - character
  - npc
---
# Name
Evarin
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Evarin

# Gossip über Evarin

# Meinung von Ludwig über Evarin