---
tags:
  - character
  - npc
---
# Name
Archimedes
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Archimedes

# Gossip über Archimedes

# Meinung von Ludwig über Archimedes