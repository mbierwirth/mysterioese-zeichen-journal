---
tags: character, pc
---
# Name
- Ferrum Degel (Nachname inkorrekt), Eller Garik, Meister Eisenhand 
# Aussehen

# Heimat

# Beruf

# Fähigkeiten

# Items

# Personen
## Familie

## Freunde

## Party

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Neldon

# Gossip über Neldon

# Meinung von Ludwig über Neldon