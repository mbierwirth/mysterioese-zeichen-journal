---
tags:
  - character
  - npc
---
# Name
Mrs Theodora
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Mrs Theodora

# Gossip über Mrs Theodora

# Meinung von Ludwig über Mrs Theodora