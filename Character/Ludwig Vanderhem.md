---
tags:
  - character
  - pc
aliases:
  - Mindslayer
---
# Name
- Ludwig Vanderhem
- Mumientöter, Mindslayer
# Aussehen
![[Ludwig Vanderhem.png]]
# Herkunft 
[[Bremwede]] 
# Beruf 
- Arbeitet in Familienunternehmen [[Vanderhem Logistics]] 
- Nebenberuflich erleichtert er reiche Menschen um ihr Vermögen
- Modelberater von [[Mirul]]
# Fähigkeiten

# Items
- Kette [[Amulet Of Proof Against Detection And Location]]
- Buch [[Von alten Göttern und Zivilisationen]]
- Wahrsageknochen [[Bones of the Dying One]]
# Personen
## Familie
- Vater [[Vater Vanderhem]]
- Mutter [[Emilia Vanderhem]] geborene Drachentöter
- Oma mütterlicherseits [[Brenda Drachentöter]]
- Schwester [[Cecilia Vanderhem]]
- Haustier [[Archimedes]]
## Freunde
- Kapitän bei [[Vanderhem Logistics]] [[Wolf Vanderlein]]
- Partner in Crime [[Phineas Fidelius Philemon]]
## Party
- [[Cali Bloom]]
- [[Mirul]]
- [[Neldon]]
- [[Yaku Purisqan]]
## Bekannte
### Geschaftspartner 
- [[Theodor Großfang]]
- [[Angelika Bäcker]]
- [[Frank Bäcker]]
### Klassenkamerade
- [[Kynan Tansen]]
- [[Adelhold]]
- [[Carl Erich]]
- [[Peter Patrick]]
- [[Anne Cathryn]]
- [[Cerit]]
### Professoren
- [[Alexandra Hauser]]
- [[Benjamin Klapper]]
- [[Dominik Nobel]]
## Feinde
- [[Die graue Hand]]
- [[Keiros]]
# Verbindungen
[[Vanderhem Logistics]], [[Kaufmannsschule]]
# Interessen

# Gossip

# Vergangenheit
## Vor der Campaign 

## Seit der Campaign

# Motivation






