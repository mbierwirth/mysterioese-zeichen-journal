---
tags: character, npc
---
# Name
Failass, Kailass
# Aussehen

# Heimat
[[Waringen]]
# Beruf

# Personen
## Freunde

## Bekannte

## Feinde
[[Keiros]]
# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
[[Keiros]] und ich waren mächtige Wesen in zwei verschiedenen Welten- Er im [[Feywild]], ich in dieser Welt. Als er die Chance sah bei einer Annäherung der zwei Welten mehr Macht zu erlangen und sich in einer Position als Anker zwischen ihnen zu etablieren wollte er diese mit allr Macht ergreifen. Ich und einige andere, im [[Feywild]] sowie in dieser Welt stellten uns gegen seine Pläne. Er hörte nicht auf logische Gründe, Bitte- selbst von engsten Vertrauten. Also griffen wir, nach langer Zeit und großen Bemühungen auch zu Waffen die ich nie einsetzen wollte: es wurde zu einem Krieg. Doch ich sah keine andere Möglichkeit seinen Hunger einzugrenzen. Viele meiner Mitstreiter wurden schlimmer gestraft als ich. Einige sind tot und unmöglich wiederzuerlangen, einige in ewigen Qualen gefangen, ich wurde lediglich in die Verbannung geschickt. Nun hoffe ich meine Macht wiederzuerlangen um das richtige Machtverhältnis wieder herzustellen und [[Keiros]] an seinen Platz zu verweisen.
## Seit Beginn der Campaign

# Meinung von Failass
Hass gegenüber [[Keiros]]
# Gossip über Failass

# Meinung von Ludwig über Failass