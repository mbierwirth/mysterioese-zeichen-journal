---
tags:
  - character
  - npc
---
# Name
Rachelia
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Rachelia

# Gossip über Rachelia

# Meinung von Ludwig über Rachelia