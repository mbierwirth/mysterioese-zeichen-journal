---
tags:
  - character
  - npc
---
# Name
Atlas
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Atlas

# Gossip über Atlas

# Meinung von Ludwig über Atlas