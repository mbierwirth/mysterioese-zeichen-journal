---
tags:
  - character
  - npc
---
# Name
Selene
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Selene

# Gossip über Selene

# Meinung von Ludwig über Selene