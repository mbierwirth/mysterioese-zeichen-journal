---
tags: character, npc
---
# Name
Hattie
# Aussehen
Gnom
# Heimat

# Beruf
Personalleiter bei Mechenergy
# Personen
## Feinde
[[Ludwig Vanderhem]], [[Cali Bloom]]
# Verbindungen
[[Mechenergy]]
# Interessen

# Motivation

# Vergangenheit

## Seit Beginn der Campaign
[[Hattie]] kontaktiert [[Neldon]] im zuge von [[Quests/Quest Der beste Schmied]] für Mechenergy zu arbeiten. Dabei droht sie, dass [[Neldon]]s Familie ansonsten etwas zustoßen könnte. [[Ludwig Vanderhem]] und [[Cali Bloom]] brechen bei ihr ein, erfahren über ihr Laster und schüchtern sie (hoffentlich) mit einer Drohung unsererseits ein.
# Meinung von Hattie

# Gossip über Hattie
Sie ließt und schreibt eigene Schundromane
# Meinung von Ludwig über Hattie