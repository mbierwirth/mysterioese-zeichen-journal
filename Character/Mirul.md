---
tags: character, pc
---
# Name
Mirul
# Aussehen

# Heimat

# Beruf

# Fähigkeiten

# Items
[[Miruls Bild]]
# Personen
## Familie

## Freunde

## Party

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Mirul

# Gossip über Mirul

# Meinung von Ludwig über Mirul