---
tags:
  - character
  - npc
---
# Name
Phineas Fidelius Philemon
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Phineas Fidelius Philemon

# Gossip über Phineas Fidelius Philemon

# Meinung von Ludwig über Phineas Fidelius Philemon