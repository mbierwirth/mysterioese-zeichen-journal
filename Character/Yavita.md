---
tags: character, npc, gott
---
# Name
Yavita
# Aussehen
Vollkommen in schwarz gekleidet
# Heimat
Am starksten in [[Die Verlassenen Länder]] angebetet. [[Ludwig Vanderhem]] hat sie auch einmal in [[Ludritz]] gesehn.
# Beruf
Göttin des Todes
# Personen
## Freunde
[[Lydia]], [[Ganimedes]]
## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Wie die anderen Götter dieser Welt beginnt sie langsam in Vergessenheit zu geraten. Aktuell ist ihr mächtigster Bewahrer [[Ganimedes]]
## Seit Beginn der Campaign
Beginnt sich [[Ludwig Vanderhem]] und [[Mirul]] zu zeigen nachdem beide in das [[Nekronomicon (Lydias Tagebuch)]] geschaut haben. 
# Meinung von Yavita

# Gossip über Yavita

# Meinung von Ludwig über Yavita
[[Ludwig Vanderhem]] hält an sich wenig von Göttern. Unter den Göttern ist sie ihm angenehm.