---
tags:
  - character
  - npc
---
# Name
Altea
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Altea

# Gossip über Altea

# Meinung von Ludwig über Altea