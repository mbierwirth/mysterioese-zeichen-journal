---
tags:
  - character
  - npc
---
# Name
Sanja
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Freundin von [[Cali Bloom]] und [[Iandir]] untersucht die [[Feywild]]portale

## Seit Beginn der Campaign

# Meinung von Sanja

# Gossip über Sanja

# Meinung von Ludwig über Sanja