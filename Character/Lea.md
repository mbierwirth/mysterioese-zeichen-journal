---
tags:
  - character
  - npc
---
# Name
Lea
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Lea

# Gossip über Lea

# Meinung von Ludwig über Lea