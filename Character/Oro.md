---
tags: character, npc
---
# Name
Oro
# Aussehen

# Heimat
[[Die Verlassenen Länder]]
# Beruf

# Personen
## Freunde
Frau und einen Sohn
## Bekannte
[[Nachthexe]]
# Motivation
So arm, dass er von [[Quest ein Schatz im Sumpf]] dazu veranlasst ist mit seiner Familie diesen Schatz zu Suchen
# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign
[[Quest ein Schatz im Sumpf]]
# Meinung von Ludwig über Oro
Ein armer Kerl der versucht so gut es geht um seine Familie zu sorgen.