---
tags:
  - character
  - npc
---
# Name
Wolf Vanderlein
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Wolf Vanderlein

# Gossip über Wolf Vanderlein

# Meinung von Ludwig über Wolf Vanderlein