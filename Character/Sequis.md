---
tags:
  - character
  - npc
---
# Name
Sequis
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Sequis

# Gossip über Sequis

# Meinung von Ludwig über Sequis