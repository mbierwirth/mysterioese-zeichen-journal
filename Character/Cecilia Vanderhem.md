---
tags: character, npc
---
# Name
Cecilia Vanderhem
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
Wird gerade beim Druiden Zirkel das [[Grüne Band]] ausgebildet
# Personen
## Familie
- Vater [[Vater Vanderhem]]
- Mutter [[Emilia Vanderhem]] geborene Drachentöter
- Oma mütterlicherseits [[Brenda Drachentöter]]
- Bruder [[Ludwig Vanderhem]]
- Haustier [[Archimedes]]
## Freunde
- Freundinnen[[Mia]], [[Lea]], [[Cassandra]] 
## Bekannte

## Feinde

# Verbindungen
[[Grüne Band]]
# Interessen
- Malt gerne 
# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Cecilia Vanderhem

# Gossip über Cecilia Vanderhem

# Meinung von Ludwig über Cecilia Vanderhem
Musste nach dem Tod von [[Cecilia Vanderhem]] beschützt werden. Soll aus den finanziellen Schwierigkeiten von [[Vanderhem Logistics]] herausgehalten werden