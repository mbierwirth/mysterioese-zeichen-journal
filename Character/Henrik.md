---
tags: character, npc
---
# Name
Henrik
# Aussehen

# Heimat

# Beruf
Druide in [[Bremwede]]
# Personen
## Freunde
[[Erkan]]
## Bekannte
 [[Fabia Sandig]], [[Emilia Vanderhem]], [[Cecilia Vanderhem]], [[Vater Vanderhem]], [[Ludwig Vanderhem]], [[Mirul]]
## Feinde

# Verbindungen
Duiden Zirkel in [[Bremwede]]
# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Er war der Schüler von [[Erkan]]. Dieser schloss ihn (wie [[Emilia Vanderhem]] [[Fabia Sandig]]) an der Teilnahme der geheimen Quest aus. [[Emilia Vanderhem]] letzte Notiz an [[Fabia Sandig]] beschreibt, dass sie sich von ihm fernhalten soll
## Seit Beginn der Campaign
Er unterstützt [[Mirul]] und [[Ludwig Vanderhem]] bei [[Quest Eine Statur von Mirul]]. Er startet [[Quest Ein Schatten über Vanderhem]]
# Meinung von Henrik

# Gossip über Henrik

# Meinung von Ludwig über Henrik