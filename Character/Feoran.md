---
tags:
  - character
  - npc
---
# Name
Feoran
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Feoran

# Gossip über Feoran

# Meinung von Ludwig über Feoran