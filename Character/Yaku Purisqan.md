---
tags:
  - character
  - npc
---
# Name
Yaku Purisqan
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Yaku Purisqan

# Gossip über Yaku Purisqan

# Meinung von Ludwig über Yaku Purisqan