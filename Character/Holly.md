---
tags:
  - character
  - npc
---
# Name
Holly
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Holly

# Gossip über Holly

# Meinung von Ludwig über Holly