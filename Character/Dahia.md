---
tags:
  - character
  - npc
---
# Name
Dahia
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Dahia

# Gossip über Dahia

# Meinung von Ludwig über Dahia