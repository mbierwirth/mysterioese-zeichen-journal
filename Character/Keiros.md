---
tags:
  - character
  - npc
---
# Name
Keiros
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Keiros

# Gossip über Keiros

# Meinung von Ludwig über Keiros