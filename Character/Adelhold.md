---
tags: character, npc
---
# Name
Adelhold
# Aussehen

# Heimat
[[Bremwede]]
# Beruf

# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Kynan Tansen]]
- [[Carl Erich]]
- [[Peter Patrick]]
- [[Anne Cathryn]]
- [[Cerit]]
### Professoren
- [[Alexandra Hauser]]
- [[Benjamin Klapper]]
- [[Dominik Nobel]]
## Freunde

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Ist mit [[Ludwig Vanderhem]], [[Kynan Tansen]], [[Carl Erich]], [[Peter Patrick]], [[Anne Cathryn]] und [[Cerit]] zur [[Kaufmannsschule]] gegangen.
## Seit Beginn der Campaign

# Meinung von Adelhold

# Gossip über Adelhold
Sie soll gerne Erpressung nutzen und für gute Noten auch schon einen Professor verprügelt haben.
# Meinung von Ludwig über Adelhold