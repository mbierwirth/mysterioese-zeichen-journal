---
tags: character, npc
---
# Name
Frank Bäcker
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
Manager des [[Kaufhaus am Markt]]
# Personen
## Freunde
- [[Angelika Bäcker]]
- [[Vater Vanderhem]], [[Ludwig Vanderhem]]
## Bekannte

## Feinde

# Verbindungen
[[Kaufhaus am Markt]]
# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Frank Bäcker

# Gossip über Frank Bäcker

# Meinung von Ludwig über Frank Bäcker