---
tags:
  - character
  - npc
---
# Name
Luryn
# Aussehen
Ork
# Heimat

# Beruf

# Personen
## Familie
- Mutter von [[Mirul]]
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Luryn

# Gossip über Luryn

# Meinung von Ludwig über Luryn