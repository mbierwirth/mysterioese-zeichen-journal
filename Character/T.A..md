---
tags:
  - character
  - npc
---
# Name
T.A.
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von T.A.

# Gossip über T.A.

# Meinung von Ludwig über T.A.