---
tags: character, npc
---
# Name
Emilia Vanderhem geborene Drachentöterin
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
Druidin im Zirkel [[Grüne Band]]
# Items
- Kette [[Amulet Of Proof Against Detection And Location]]
# Personen

## Familie
- Mutter [[Brenda Drachentöter]]
- Mann [[Vater Vanderhem]]
- Kinder [[Ludwig Vanderhem]] und [[Cecilia Vanderhem]]
## Freunde
Schülerin [[Fabia Sandig]]
## Bekannte

## Feinde

# Verbindungen
[[Grüne Band]]
# Interessen
Kann mit Tieren sprechen
# Motivation

# Vergangenheit
## Vor der Campaign 
Tochter eines wenig erfolgreichen Kaufmanns und der Magiern [[Brenda Drachentöter]]. Sollte wie ihre Mutter zu einer Magierin ausgebildet werden. Ihre Talente entsprachen aber dann doch eher den der Druiden
## Seit Beginn der Campaign

# Meinung von Emilia Vanderhem

# Gossip über Emilia Vanderhem

# Meinung von Ludwig über Emilia Vanderhem