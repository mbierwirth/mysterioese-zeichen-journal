---
tags: character, npc
---
# Name
The Dying One
# Aussehen
Ein in schwarze Lumpen gehülltes Skellet
# Heimat
[[Der begrabene Turm]]
# Beruf

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Möglicherweise mitverantwortlich für die Massaker im [[Der begrabene Turm]]
## Seit Beginn der Campaign
Getötet im Kampf. Er bildet [[Bones of the Dying One]]. Der Kampf mit ihm ist Auslöser für [[Quest Eine Statur von Mirul]] und [[Quest gezeichnet vom Tod]]
# Meinung von The Dying One

# Gossip über The Dying One

# Meinung von Ludwig über The Dying One