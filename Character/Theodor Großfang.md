---
tags: character, npc
---
# Name
Theodor Großfang
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
Bänker 
# Personen
## Freunde
- [[Vater Vanderhem]], [[Ludwig Vanderhem]]
## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Theodor Großfang

# Gossip über Theodor Großfang

# Meinung von Ludwig über Theodor Großfang
Hat der Familie einen großen Dienst erwiesen als er geholfen hat die Firma über Wasser zu halten