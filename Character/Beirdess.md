---
tags: character, npc, feywild, god
---
# Name
Beirdess
# Aussehen
schwarze katze mit gelben augen
# Heimat
[[Feywild]], [[Ludritz]]
# Beruf
Göttin im [[Feywild]]
# Personen
## Freunde
- Katze von [[Martha]]
- Befreundete [[feywild]] Götter alle
## Bekannte

## Feinde

# Verbindungen

# Interessen
Personifikation der liebe und familie, im bereich der schatten und mond
# Motivation

# Vergangenheit
## Vor der Campaign 
Chillt als Katze von [[Martha]] in [[Ludritz]]
## Seit Beginn der Campaign

# Meinung von Beirdess

# Gossip über Beirdess
Gilt als friedlich und kümmernd
# Meinung von Ludwig über Beirdess