---
tags:
  - character
  - npc
---
# Name
Tibald
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Tibald

# Gossip über Tibald

# Meinung von Ludwig über Tibald