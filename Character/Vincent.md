---
tags: character, pc
---
# Name
Myrul, [[Mirul]], Vincent, Buggy the Badger
# Aussehen
Bewohnt [[Mirul]] Körper
# Heimat

# Beruf
Dieb
# Fähigkeiten
Alle von [[Mirul]]
# Items

# Personen
## Familie

## Freunde

## Party
Ludolf (Chef)
## Bekannte

## Feinde
[[Big Chunky]]
# Verbindungen

# Interessen
Möchte wieder einen eigenen Körper
# Motivation
Aktiv daran interessiert die Siegel in [[Keiros]] Tempel zu zerstören, das was ihn an sein altes Leben bindet.
# Vergangenheit
## Vor der Campaign 
Versuchte mit seiner group [[Keiros]] Tempel aka [[Lil Chunky]] und [[Big Chunky]] zu Hause zu überfallen und die Sigel des Portals zum [[Feywild]] zu zerstören. Es wurden Ihnen dafür 10000 gold versprochen von [[T.A.]]?. Ist dabei in den Spiegelraum gegangen und sein Geist wurde darin gefangen. Seine Gruppe sind im Kampf mit [[Big Chunky]] gestorben. Sein Körper ebenfalls.
## Seit Beginn der Campaign
Nahm Platz in [[Mirul]] Körper Platz, nachdem [[Mirul]] in den Spiegelraum gegangen ist. Danach widersetzte er sich mit großem elan den Wünschen der Party und zerstörte zumindestens teilweise das Hauptsiegel zum [[Feywild]] Portal. Wurde danach in den Raum geschickt wurde aber nicht erneut im Spiegel gefangen. Wir Transferieren ihn in den Badger. Im Anschluss gehen wir zu [[Ganimedes]] um ihm einen längerfristigen Körper zu organisieren. 

# Meinung von Myrul

# Gossip über Myrul

# Meinung von Ludwig über Myrul