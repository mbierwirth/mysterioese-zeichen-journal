---
tags: character, npc
---
# Name
Dominik Nobel
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
- Professor für Management & Leadership der [[Kaufmannsschule]]
- Stellvertretender Leiter der Kaufmansschule 
# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Kynan Tansen]]
- [[Adelhold]]
- [[Carl Erich]]
- [[Peter Patrick]]
- [[Anne Cathryn]]
- [[Cerit]]
- [[Benjamin Klapper]]
- [[Alexandra Hauser]]
## Freunde

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Professorin von [[Ludwig Vanderhem]], [[Adelhold]], [[Carl Erich]], [[Peter Patrick]], [[Anne Cathryn]], [[Cerit]] und [[Kynan Tansen]]
## Seit Beginn der Campaign

# Meinung von Dominik Nobel

# Gossip über Dominik Nobel
Bekannt dafür immer seinen Willen zu kriegen
# Meinung von Ludwig über Dominik Nobel
Nach außen charmanter immer freundlicher Typ. Bevorzugt einige Leute aber so unglaublich