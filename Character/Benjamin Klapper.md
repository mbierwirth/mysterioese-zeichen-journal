---
tags:
  - character
  - npc
---
# Name
Benjamin Klapper
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
- Professor für Firmengründung & Strukturen an der [[Kaufmannsschule]]
- Mehrfacher gescheiterter Startup Gründer
# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Kynan Tansen]]
- [[Adelhold]]
- [[Carl Erich]]
- [[Peter Patrick]]
- [[Anne Cathryn]]
- [[Cerit]]
- [[Alexandra Hauser]]
- [[Dominik Nobel]]
## Freunde

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Professorvon [[Ludwig Vanderhem]], [[Adelhold]], [[Carl Erich]], [[Peter Patrick]], [[Anne Cathryn]], [[Cerit]] und [[Kynan Tansen]]
## Seit Beginn der Campaign

# Meinung von Benjamin Klapper

# Gossip über Benjamin Klapper
Verprasst sein geerbtes Geld, durch Investitionen in super sichere Startup Tipps
# Meinung von Ludwig über Benjamin Klapper
Hat immer den super coolen Typen gespielt