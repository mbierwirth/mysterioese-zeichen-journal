---
tags:
  - character
  - npc
---
# Name
Markus
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Markus

# Gossip über Markus

# Meinung von Ludwig über Markus