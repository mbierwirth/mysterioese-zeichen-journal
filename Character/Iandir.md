---
tags:
  - character
  - npc
---
# Name
Iandir
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Bekannter von [[Cali Bloom]]. Erforscht die [[Feywild]] Portale
## Seit Beginn der Campaign

# Meinung von Iandir

# Gossip über Iandir

# Meinung von Ludwig über Iandir