---
tags: character, npc
---
# Name
Fabia Sandig
# Aussehen
Halbling
# Heimat
[[Ludritz]], [[Bremwede]]
# Beruf
Druidin
# Personen
## Freunde
[[Emilia Vanderhem]], [[Mirul]]
## Bekannte
[[Selene]], [[Keiros]], [[Atlas]]
## Feinde
[[Henrik]]
# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
War die Schülerin von [[Emilia Vanderhem]] als [[Ludwig Vanderhem]] noch ein Kind war. [[Mirul]] hat ihr geholfen und ihr dafür einen [[Sending Stone Fabia]] gegeben.
## Seit Beginn der Campaign
[[Mirul]] ruft sie an als die Tür der [[Kirche mit Herztür]] nicht aufgemacht bekommt. Sie kommt und zerstört den inhalt der Kirche. Ist dann aber nicht mehr in der Lage [[Neldon]] von seinem Fluch zu befreien.
# Meinung von Fabia Sandig

# Gossip über Fabia Sandig

# Meinung von Ludwig über Fabia Sandig