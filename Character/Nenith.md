---
tags: character, npc, feywild, god
---
# Name
- Nenith
# Aussehen
orange katze mit grünen augen 
# Heimat
[[Feywild]], [[Ludritz]]
# Beruf
Göttin im [[Feywild]]
# Personen
## Freunde
- Katze von [[Martha]]
- Befreundete [[feywild]] Götter [[Beirdess]], [[Feoran]], [[Hyrath]] und [[Sequis]] 
## Bekannte

## Feinde

# Verbindungen

# Interessen
Personifikation wildniss, stärke und freier geist, im licht der sonne
# Motivation

# Vergangenheit
## Vor der Campaign 
Chillt als Katze von [[Martha]] in [[Ludritz]]
## Seit Beginn der Campaign

# Meinung von Nenith

# Gossip über Nenith

# Meinung von Ludwig über Nenith