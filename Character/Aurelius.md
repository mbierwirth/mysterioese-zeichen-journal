---
tags:
  - character
  - npc
---
# Name
Aurelius
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Aurelius

# Gossip über Aurelius

# Meinung von Ludwig über Aurelius