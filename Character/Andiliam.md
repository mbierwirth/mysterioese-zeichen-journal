---
tags: character, npc, feywild, god
---
# Name
Andiliam
# Aussehen
hellgrau-dunkelgrau gescheckte katze mit blauen augen
# Heimat
[[Feywild]], [[Ludritz]]
# Beruf
Göttin im [[Feywild]]
# Personen
## Freunde
- Katze von [[Martha]]
- Befreundete [[feywild]] Götter [[Feoran]], [[Keiros]] und [[Evarin]]
## Bekannte

## Feinde

# Verbindungen

# Interessen
personifikation der hoffnung und geschichten
# Motivation

# Vergangenheit
## Vor der Campaign 
Chillt als Katze von [[Martha]] in [[Ludritz]]
## Seit Beginn der Campaign

# Meinung von Andiliam

# Gossip über Andiliam

# Meinung von Ludwig über Andiliam