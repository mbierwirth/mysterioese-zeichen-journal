---
tags: character, npc
---
# Name
Cerit
# Aussehen

# Heimat
[[Bremwede]]
# Beruf

# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Adelhold]]
- [[Carl Erich]]
- [[Peter Patrick]]
- [[Anne Cathryn]]
- [[Kynan Tansen]]
### Professoren
- [[Alexandra Hauser]]
- [[Benjamin Klapper]]
- [[Dominik Nobel]]
## Freunde

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Ist mit [[Ludwig Vanderhem]], [[Kynan Tansen]], [[Carl Erich]], [[Peter Patrick]], [[Anne Cathryn]] und [[Adelhold]] zur [[Kaufmannsschule]] gegangen.
## Seit Beginn der Campaign

# Meinung von Cerit

# Gossip über Cerit
Jeder weiß, dass ihr Vater lieber einen Jungen gehabt hätte
# Meinung von Ludwig über Cerit
Freundlich aber Schüchtern