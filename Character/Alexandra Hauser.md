---
tags: character, npc
---
# Name
Alexandra Hauser
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
- Professorin für Marketing & Press an der [[Kaufmannsschule]]
- External Consultant für die Mediendarstellung von Skandal Unternehmen
# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Kynan Tansen]]
- [[Adelhold]]
- [[Carl Erich]]
- [[Peter Patrick]]
- [[Anne Cathryn]]
- [[Cerit]]
- [[Benjamin Klapper]]
- [[Dominik Nobel]]
## Freunde

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Professorin von [[Ludwig Vanderhem]], [[Adelhold]], [[Carl Erich]], [[Peter Patrick]], [[Anne Cathryn]], [[Cerit]] und [[Kynan Tansen]]
## Seit Beginn der Campaign

# Meinung von Alexandra Hauser

# Gossip über Alexandra Hauser

# Meinung von Ludwig über Alexandra Hauser