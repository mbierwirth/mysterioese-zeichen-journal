---
tags:
  - character
  - npc
---
# Name
Sabrina
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Sabrina

# Gossip über Sabrina

# Meinung von Ludwig über Sabrina