---
tags:
  - character
  - npc
---
# Name
Martha
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Martha

# Gossip über Martha

# Meinung von Ludwig über Martha