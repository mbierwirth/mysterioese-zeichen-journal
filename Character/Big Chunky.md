---
tags:
  - character
  - npc
---
# Name
Big Chunky
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Big Chunky

# Gossip über Big Chunky

# Meinung von Ludwig über Big Chunky