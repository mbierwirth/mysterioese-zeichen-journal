---
tags: character, npc
---
# Name
Anne Cathryn
# Aussehen

# Heimat
[[Bremwede]]
# Beruf
Leiterin des Kleidungsunternehmens [[Stoff der Träume]]
# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Adelhold]]
- [[Carl Erich]]
- [[Peter Patrick]]
- [[Cerit]]
### Professoren
- [[Alexandra Hauser]]
- [[Benjamin Klapper]]
- [[Dominik Nobel]]
## Freunde
- Mit [[Kynan Tansen]] zusammen
## Feinde

# Verbindungen
[[Stoff der Träume]]
# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Ist mit [[Ludwig Vanderhem]], [[Adelhold]], [[Carl Erich]], [[Peter Patrick]], [[Kynan Tansen]] und [[Cerit]] zur [[Kaufmannsschule]] gegangen. Sie war zu dem Zeitpunkt ungewöhnlich jung.
## Seit Beginn der Campaign

# Meinung von Anne Cathryn

# Gossip über Anne Cathryn

# Meinung von Ludwig über Anne Cathryn
Ohne Zweifel intelligent, dafür aber mindestens doppelt so arrogant