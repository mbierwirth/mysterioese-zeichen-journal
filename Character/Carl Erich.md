---
tags: character, npc
---
# Name
Carl Erich
# Aussehen

# Heimat
[[Bremwede]]
# Beruf

# Personen
## Bekannte
- [[Ludwig Vanderhem]]
- [[Adelhold]]
- [[Kynan Tansen]]
- [[Peter Patrick]]
- [[Anne Cathryn]]
- [[Cerit]]
### Professoren
- [[Alexandra Hauser]]
- [[Benjamin Klapper]]
- [[Dominik Nobel]]
## Freunde

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 
Ist mit [[Ludwig Vanderhem]], [[Kynan Tansen]], [[Adelhold]], [[Peter Patrick]], [[Anne Cathryn]] und [[Cerit]] zur [[Kaufmannsschule]] gegangen.
Zweiter Sohn eines der reichsten Kaufmänners Bremwedes
## Seit Beginn der Campaign

# Meinung von Carl Erich

# Gossip über Carl Erich

# Meinung von Ludwig über Carl Erich
Versucht sich beliebt zu machen. Ansonsten ganz nett