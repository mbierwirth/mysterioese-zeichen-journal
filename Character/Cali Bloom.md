---
tags:
  - character
  - npc
---
# Name
Cali Bloom
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Cali Bloom

# Gossip über Cali Bloom

# Meinung von Ludwig über Cali Bloom