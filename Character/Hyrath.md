---
tags:
  - character
  - npc
---
# Name
Hyrath
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Hyrath

# Gossip über Hyrath

# Meinung von Ludwig über Hyrath