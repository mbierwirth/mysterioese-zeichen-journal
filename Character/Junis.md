---
tags:
  - character
  - pc
---
# Name
Junis
# Aussehen

# Heimat

# Beruf

# Fähigkeiten

# Items

# Personen
## Familie

## Freunde

## Party

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Junis

# Gossip über Junis

# Meinung von Ludwig über Junis