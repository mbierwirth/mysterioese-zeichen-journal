---
tags: character, npc
---
# Name
Angelika Bäcker
# Aussehen

# Heimat
[[Bremwede]]
# Beruf

# Personen
## Freunde
- Mann [[Frank Bäcker]]
- Gouvernante von [[Cecilia Vanderhem]]
## Bekannte

## Feinde

# Verbindungen
[[Kaufhaus am Markt]]
# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Angelika Bäcker

# Gossip über Angelika Bäcker

# Meinung von Ludwig über Angelika Bäcker