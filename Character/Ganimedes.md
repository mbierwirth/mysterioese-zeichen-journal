---
tags: character, npc
---
# Name
Ganimedes
# Aussehen
Halbling, blond mit locken, im zauberer kostüm
# Heimat
[[Die Verlassenen Länder]]
# Beruf
Nekromant. Er verteilt Schutz im Gegenzug für die Körper der Toten. 
# Personen
## Freunde
[[Atlas]], [[Lydia]]
## Bekannte

## Feinde

# Verbindungen
 [[Yavita]] die Göttin des Todes
# Interessen
Möchte nicht, dass ihn viele Leute besuchen
# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Ganimedes

# Gossip über Ganimedes

# Meinung von Ludwig über Ganimedes