---
tags:
  - character
  - npc
---
# Name
Lil Chunky
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von Lil Chunky

# Gossip über Lil Chunky

# Meinung von Ludwig über Lil Chunky