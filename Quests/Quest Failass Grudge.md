---
tag: quest, open
---
# Relevante Sessions
[[Session 31]], [[Session 32]]
# Involvierte Charaktere
[[Keiros]], [[Failass]], [[Neldon]], [[Ludwig Vanderhem]]
# Bisheriger Stand
[[Ludwig Vanderhem]] wird von einer ominösen Stimme in seinem Kopf bezüglich [[Keiros]] gefragt.  Er behauptet von [[Keiros]] zerstört worden zu sein, er hätte sogar seinen Namen ausgelöscht. Er will auf starke nachfrage [[Failass]] oder Gerechtigkeit genannt werden. [[Keiros]] habe Familien zerrissen und Freundschaften zerstört um dahin zu kommen wo er jetzt ist. Er möchte das [[Ludwig Vanderhem]] und [[Neldon]] sich nicht gegen Ihn stellen und will zu einem Gegenpol zu [[Keiros]] Macht aufsteigen und möchte vermutlich dafür Hilfe.
[[Failass]] hilft (ohne Gegenleistung) das Boot vor [[Ludritz]] aus einem Sturm zu manövrieren, indem er einen Kraken sendet. Nach [[Ludritz]] bringt er das Boot nicht mehr, da er von [[Ludwig Vanderhem]] Informationen über [[Keiros]] Beziehungen zu den anderen Göttern verlangt die Ihm [[Ludwig Vanderhem]] nicht geben möchte. Er wurde in [[Orte/Keiros Tempel]] von der Materialplane in die Hölle ([[Avernus]]) verbannt.