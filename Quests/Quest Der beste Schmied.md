---
tag: quest
---
# Relevante Sessions
[[Session 37]], [[Session 38]], [[Session 39]], [[Session 40]]
# Involvierte Charaktere
[[Neldon]], [[Mechenergy]]
# Bisheriger Stand
[[Neldon]] bekommt einen Brief von der rassistischen Personalleiterin von [[Mechenergy]][[Hattie]] bezüglich eines Jobangebots bei [[Mechenergy]]. Ansonsten könnte seine Familie zu schaden kommen. [[Neldon]] einigt sich mit ihr auf eine Woche Probearbeiten. [[Ludwig Vanderhem]] und [[Cali Bloom]] brechen in ihr Haus ein und finden die Namen von drei anderen Kontaktierten Gnomen darunter [[Molly]].  [[Neldon]] und der Rest der Party macht sich auf den Weg zu [[Mechenergy]] Nach langem Weg erreichen wir das [[Mechenergy]] Quatier in den Bergen. 
