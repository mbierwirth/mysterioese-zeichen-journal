---
tag: quest, open
---
# Relevante Sessions
[[Session 32]]
# Involvierte Charaktere
[[Ludwig Vanderhem]], [[Emilia Vanderhem]], [[Fabia Sandig]]
# Bisheriger Stand
[[Emilia Vanderhem]] ist vor 10 Jahren unter mysteriösen Umständen gestorben. Zuvor war [[Fabia Sandig]] genervt bei den Vanderhems aufgetaucht, da sie zurückgelassen und ignoriert wurde. Ab dem Krankheitsbeginn von [[Emilia Vanderhem]] war [[Fabia Sandig]] einmal da und danach nie wieder
