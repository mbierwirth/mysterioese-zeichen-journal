---
tags:
  - quest
---
# Relevante Sessions
[[Session 54]], [[Session 56]]
# Involvierte Charaktere
[[Cali Bloom]]
# Bisheriger Stand
[[Cali Bloom]] fand ein Diadem, welches ihr magische Fähigkeiten und eine Direktverbindung einer uns bisher unbekannten Gottheit ermöglicht


Gestellte Fragen:
1. Sorgst du mit deinen Antworten für den für uns bestmöglichsten Ausgang der kommenden Ereignisse, soweit es dir möglich ist? Nein
2. Sorgst du mit deinen Antworten für den für das Militär von Salost bestmöglichsten Ausgang der kommenden Ereignisse, soweit es dir möglich ist?
3. Finden wir etwas für uns Lohnenswertes (entweder Dinge oder Informationen, die uns auf unserem Weg nützlich sein können), wenn wir weiter auf der Insel investigieren?
4. dürfen wir sie weiter kontaktieren? Ja
5. kommen wir ohne probleme von der insel, auch im bezug auf die geschehnisse letzte nacht? Weiß ich nicht
6. Hast du einen schrein in der kirche? nein
7. hat mechenergy uns über die knöpfe im ohr abgehört? ja
8.  ist miss theodora noch aktiv hinter uns her? ja
9.  sind wir mehr dadurch in gefahr, dass wir stivan die juwelen gegeben haben, als wenn wir sie selber behalten oder liegen gelassen hätten? ja