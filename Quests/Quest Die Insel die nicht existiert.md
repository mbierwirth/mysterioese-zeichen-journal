---
tags:
  - quest
---
# Relevante Sessions

# Involvierte Charaktere

# Bisheriger Stand
Nach unserem Höllenausflug landen wir auf einer Insel im Nachbarland [[Salost]]. Die Insel ist bewohnt von mächtigen magisch begabten einwohnern (mechana, arcana und holy). Sie scheinen für die Regierung von [[Salost]] zu arbeiten und wollen uns so schnell wie möglich loswerden. Wir werden durchgehend gebabysittet von [[Sabrina]]. Sie halten uns fern von einem gebäude komplex im Norden der Insel. In diesem Gebäude scheint es ein weiteres Höllenportal zu geben. Dieses ist umgeben von einem Antimagischem Feld, welches einer bestimmten Gottheit gewidmet ist und nur ihre magie zulässt. [[Ludwig Vanderhem]] und [[Mirul]] "trauen" sich nicht das Feld zu betreten. 