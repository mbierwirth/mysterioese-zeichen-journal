---
tag: quest, closed
---
# Relevante Sessions
[[Session 33]], [[Session 34]], [[Session 35]], [[Session 36]]
# Involvierte Charaktere
[[Ganimedes]], [[Vincent]], [[Lydia]], [[Mirul]], [[Ludwig Vanderhem]]
# Bisheriger Stand
Im Austausch für einenen Körper für [[Vincent]] erwartet [[Ganimedes]], dass wir das Tagebuch seiner Mentorin [[Lydia]] finden und ihm bringen. Wir finden einen versteckten Turm im Sumpf. Dort finden wir in einem doppelten Boden das [[Nekronomicon (Lydias Tagebuch)]]. Wir werden angegriffen von [[The Dying One]]. Nach unserem Sieg machen wir uns auf den Rückweg. [[Ludwig Vanderhem]] und [[Mirul]] öffnen das Buch und werden mit nekromantischer Energie überströmt. Beide erfahren die dunklen Geheimnisse des Buches. [[Ludwig Vanderhem]] will daraufhin das Buch nicht [[Ganimedes]] überlassen wird aber überstimmt. [[Ganimedes]] will auch nicht, dass das Wissen des Buches in falsche Hände gelangt. 

