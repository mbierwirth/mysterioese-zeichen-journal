---
tag: quest
---
# Relevante Sessions
[[Session 35]]
# Involvierte Charaktere
[[Mirul]], [[Neldon]], [[The Dying One]]
# Bisheriger Stand
Die Charaktere die [[The Dying One]] im Kampf berührt hat, weisen schwarze Punkte von nekrotischem Gewebe an den Berührungspunkten auf. [[Ganimedes]] und [[Henrik]] bestätigen die vorläufige Harmlosigkeit.