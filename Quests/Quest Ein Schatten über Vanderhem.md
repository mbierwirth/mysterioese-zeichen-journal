---
tag: quest
---
# Relevante Sessions
[[Session 38]]
# Involvierte Charaktere
[[Henrik]], [[Ludwig Vanderhem]]
# Bisheriger Stand
[[Henrik]] erzählt [[Ludwig Vanderhem]] davon, dass seit einiger Zeit das Haus der Vanderhems von einer bösen Macht beobachtet wird und [[Vater Vanderhem]] das Haus nicht verlassen will. Die selbe Energiesignatur wie der ehemalige [[Tempel der ewigen Liebe]]. 
