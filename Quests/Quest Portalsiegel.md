---
tag: quest, open
---
# Relevante Sessions
[[Session 30]], [[Session 31]]
# Involvierte Charaktere
[[Vincent]], [[Mudwig]], [[Cali Bloom]], [[Neldon]], [[Yaku Purisqan]], [[Lil Chunky]], [[Keiros]], [[Failass]]
# Bisheriger Stand
[[Vincent]] hat mit ihrer ursprünglichen Bande versucht die Portalsiegel zu brechen. Die anderen aus ihrer Gruppe wurden von [[Big Chunky]] getötet. Ausgelöst durch [[Quest Körpertausch]]. [[Vincent]] versucht die Quest der Portalöffnung abzuschließen. [[Yaku Purisqan]] ist die lauteste Person dagegen, fängt im Kampf mit [[Vincent]] sogar zu leuchten an. Portal wird von [[Vincent]] aufgebrochen. Es führt in die Hölle ([[Avernus]]). Dahinter ist [[Failass]] verbannt gewesen.
