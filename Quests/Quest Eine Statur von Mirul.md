---
tag: quest, closed
---
# Relevante Sessions
[[Session 35]], [[Session 36]], [[Session 37]], [[Session 38]]
# Involvierte Charaktere
[[Mirul]], [[The Dying One]]
# Bisheriger Stand
[[Mirul]] wird von einer Attacke des [[The Dying One]] getroffen. Im Anschluss beginnt sie über den Laufe der Zeit immer schwerer werdende Arthrose zu bekommen. [[Ganimedes]] untersucht sie kann ihr aber nicht weiter helfen. Von [[Henrik]] erfährt sie, dass der [[Herzstein der Nighthag]] die Schwimmblase eines Wassermanns oder das Horn eines Einhorns den Fluch heilen werden kann. Ansonsten würde sie im Laufe mehrerer Wochen zu Stein werden. [[Ludwig Vanderhem]] gibt ihr den [[Herzstein der Nighthag]] und sie ist ihn freudig. 
