---
tag: quest, closed
---
# Relevante Sessions
[[Session 34]], [[Session 35]]
# Involvierte Charaktere
[[Nachthexe]], [[Oro]], [[The Dying One]]
# Bisheriger Stand
Die [[Nachthexe]] erzählte [[Oro]] von einer Festung im Sumpf in [[Die Verlassenen Länder]]. Dort sollen es immense Schätze geben. Nachdem ein Feuer einen Teil seiner Farm zerstört hat beschloss er zusammen mit seinem Sohn und seiner Frau nach der Festung zu suchen. Die Party verspricht ihm 10 Prozent des gefundenen dagegen, dass er mitkommt. Die Festung stellt sich als der Turm aus [[Quest Lydias Tagebuch]] heraus. Die Party gibt ihm im Anschluss 40 Gold