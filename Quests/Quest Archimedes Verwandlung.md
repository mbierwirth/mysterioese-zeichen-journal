---
tag: quest
---
# Relevante Sessions
[[Session 35]], [[Session 36]], [[Session 38]]
# Involvierte Charaktere
[[Archimedes]]
# Bisheriger Stand
[[Archimedes]] wird vom [[The Dying One]] getötet. Bei der nächsten Beschwörung scheint er an Lebendigkeit dazugewonnen zu haben. Er flammbiert [[Mirul]] Marshmellows und greift auch den Drachen aus [[Session 38]] mit mysteriösen Flammen an.