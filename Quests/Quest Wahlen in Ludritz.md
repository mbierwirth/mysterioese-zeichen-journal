---
tag: quest, closed
---
# Relevante Sessions
[[Session 38]]
# Involvierte Charaktere

# Bisheriger Stand
Parteien:
- Tierschutzpartei: positioniert sich gegen die Ausbreitung von [[Ludritz]]. (7%)
- [[Kelia Ammargan]] Partei: setzt sich für die Regulierung von Mechana Gegenständen ein (41%)
- [[Earl Luttmann]] Partei: steht für den Status quo und ist [[Kelia Ammargan]] direkter konkurrent (26%)
- Minderheitenpartei der Zauberer: Setzt sich für die Abschaffung von Mechana ein. Möchte die alten Arcana traditionen wieder aufleben lassen. Einzige Partei die mit den Royalköniglichenkoalieren würden (6%)
- Die Royalköniglichen: Stehen für die Erweiterung der Macht des Königs. Von den meisten anderen Parteien (und Personen) ignoriert (6%)
- Sozialversicherungspartei: Steht für mehr Soziales (10%)
- Herms Herms unabhängiger Kandidat: Ein bekannter Student aus [[Ludritz]] mit einem sehr beeindruckenden Hut (1,7%)
Es wird eine Regierung zwischen [[Kelia Ammargan]] und der Sozialversicherungsparteo geben
