---
tags: character, npc
---
# Name
{{title}}
# Aussehen

# Heimat

# Beruf

# Personen
## Freunde

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von {{title}}

# Gossip über {{title}}

# Meinung von Ludwig über {{title}}