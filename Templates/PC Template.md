---
tags: character, pc
---
# Name
{{title}}
# Aussehen

# Heimat

# Beruf

# Fähigkeiten

# Items

# Personen
## Familie

## Freunde

## Party

## Bekannte

## Feinde

# Verbindungen

# Interessen

# Motivation

# Vergangenheit
## Vor der Campaign 

## Seit Beginn der Campaign

# Meinung von {{title}}

# Gossip über {{title}}

# Meinung von Ludwig über {{title}}